package ru.t1.skasabov.tm.command.project;

import ru.t1.skasabov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    private static final String NAME = "project-create";

    private static final String DESCRIPTION = "Create new project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

}
