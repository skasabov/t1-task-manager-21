package ru.t1.skasabov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String NAME = "about";

    private static final String DESCRIPTION = "Show about program.";

    private static final String ARGUMENT = "-a";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Stas Kasabov");
        System.out.println("email: stas@kasabov.ru");
        System.out.println("email: stkasabov@yandex.ru");
    }

}
