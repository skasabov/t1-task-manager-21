package ru.t1.skasabov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private static final String NAME = "project-clear";

    private static final String DESCRIPTION = "Remove all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
