package ru.t1.skasabov.tm.command.user;

import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private static final String NAME = "login";

    private static final String DESCRIPTION = "User login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
