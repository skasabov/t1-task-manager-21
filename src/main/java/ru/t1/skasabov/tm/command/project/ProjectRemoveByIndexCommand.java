package ru.t1.skasabov.tm.command.project;

import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-remove-by-index";

    private static final String DESCRIPTION = "Remove project by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
