package ru.t1.skasabov.tm.repository;

import ru.t1.skasabov.tm.api.repository.IRepository;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    protected void removeAll(final List<M> models) {
        records.removeAll(models);
    }

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public M findOneById(final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M remove(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public void removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) throw new ModelEmptyException();
        records.remove(model);
    }

    @Override
    public void removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) throw new ModelEmptyException();
        records.remove(model);
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public int getSize() {
        return records.size();
    }

    @Override
    public Boolean existsById(final String id) {
        return findOneById(id) != null;
    }

}
