package ru.t1.skasabov.tm.api.repository;

import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    void removeById(String id);

    void removeByIndex(Integer index);

    void clear();

    int getSize();

    Boolean existsById(String id);

}
