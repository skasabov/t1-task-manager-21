package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    void updateById(String userId, String id, String name, String description);

    void updateByIndex(String userId, Integer index, String name, String description);

    void changeProjectStatusById(String userId, String id, Status status);

    void changeProjectStatusByIndex(String userId, Integer index, Status status);

}
